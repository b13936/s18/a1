console.log("Hello");

let trainer = {
	Name: "Ash Ketchum",
	Age: 10,
	Pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	Friends: [
		{
			hoenn: ["May","Max"]
			
		},
		{
			kanto: ["Brock","Misty"]
			
		},
	],
	talk: function(){
		console.log(`Pikachu! I choose You!`)
	}
}

console.log(trainer)


console.log(`Result of dot notation:`)
console.log(trainer.Name)

console.log(`Results of square bracket notation:`)
console.log(trainer[`Pokemon`])

console.log(`Result of talk method:`)
function talkmethod(){
	this.talk = function(element){
		console.log(element)
	}
	
}

let asdf = new talkmethod("Pikachu! I choose you!")
